/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jruby.rack;

import org.jruby.Ruby;
import org.jruby.runtime.builtin.IRubyObject;

/**
 *
 * @author nicksieger
 */
public class DefaultRackApplicationFactory implements RackApplicationFactory {
    public DefaultRackApplicationFactory() {
    }

    public RackApplication newApplication(String loader) {
        Ruby runtime = newRuntime();
        IRubyObject app = runtime.evalScriptlet(loader);
        app = runtime.evalScriptlet("Rack::Handler::Servlet")
            .callMethod(runtime.getCurrentContext(), "new", new IRubyObject[] {app});
        return new DefaultRackApplication(app);
    }

    public void finishedWithApplication(RackApplication app) {
    }

    public Ruby newRuntime() {
        Ruby runtime = Ruby.newInstance();
        runtime.getLoadService().require("rack");
        runtime.getLoadService().require("rack/handler/servlet");
        return runtime;
    }
}
