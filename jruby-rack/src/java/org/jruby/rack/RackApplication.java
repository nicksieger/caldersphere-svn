/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jruby.rack;

import javax.servlet.ServletRequest;

/**
 *
 * @author nicksieger
 */
public interface RackApplication {
    RackResult call(ServletRequest env);
}
