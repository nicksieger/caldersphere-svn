/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jruby.rack;

import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nicksieger
 */
public interface RackResult {
    void writeStatus(HttpServletResponse response);
    void writeHeaders(HttpServletResponse response);
    void writeBody(HttpServletResponse response);
}
