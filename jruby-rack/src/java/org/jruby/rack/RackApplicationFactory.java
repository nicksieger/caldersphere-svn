/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jruby.rack;

/**
 *
 * @author nicksieger
 */
public interface RackApplicationFactory {
    RackApplication newApplication(String loader);
    void finishedWithApplication(RackApplication app);
}
