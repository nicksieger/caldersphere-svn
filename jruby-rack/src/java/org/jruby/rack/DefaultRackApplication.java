package org.jruby.rack;

import java.io.IOException;
import javax.servlet.ServletRequest;
import org.jruby.Ruby;
import org.jruby.RubyIO;
import org.jruby.exceptions.RaiseException;
import org.jruby.javasupport.JavaEmbedUtils;
import org.jruby.runtime.Arity;
import org.jruby.runtime.Block;
import org.jruby.runtime.builtin.IRubyObject;
import org.jruby.runtime.callback.Callback;

/**
 *
 * @author nicksieger
 */
public class DefaultRackApplication implements RackApplication {
    private IRubyObject application;

    public DefaultRackApplication(IRubyObject application) {
        this.application = application;
    }

    public RackResult call(final ServletRequest env) {
        Ruby runtime = this.application.getRuntime();
        IRubyObject servlet_env = JavaEmbedUtils.javaToRuby(runtime, env);
        servlet_env.getMetaClass().defineMethod("to_io", new Callback() {
            public IRubyObject execute(IRubyObject recv, IRubyObject[] args, Block block) {
                try {
                    return new RubyIO(recv.getRuntime(), env.getInputStream());
                } catch (IOException ex) {
                    throw RaiseException.createNativeRaiseException(recv.getRuntime(), ex);
                }
            }
            public Arity getArity() {
                return Arity.NO_ARGUMENTS;
            }
        });
        IRubyObject result = this.application.callMethod(runtime.getCurrentContext(),
                "call", new IRubyObject[] { servlet_env });
        return (RackResult) JavaEmbedUtils.rubyToJava(runtime, result, RackResult.class);
    }
}
