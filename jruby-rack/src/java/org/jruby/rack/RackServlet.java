package org.jruby.rack;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nicksieger
 */
public class RackServlet extends HttpServlet {
    private RackApplicationFactory rackFactory;
    private String loader;

    public RackServlet() {
        this(new DefaultRackApplicationFactory());
    }

    public RackServlet(RackApplicationFactory rackFactory) {
        this.rackFactory = rackFactory;
    }

    @Override
    public void init() throws ServletException {
        this.loader = getServletContext().getInitParameter("rack.loader");
    }
    
    @Override
    public void service(ServletRequest request, ServletResponse response)
        throws ServletException, IOException {
        process((HttpServletRequest) request, (HttpServletResponse) response);
    }

    public void process(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        RackApplication app = rackFactory.newApplication(loader);
        try {
            RackResult result = app.call(request);
            result.writeStatus(response);
            result.writeHeaders(response);
            result.writeBody(response);
        } catch (RuntimeException re) {
            throw new ServletException("Error processing request", re);
        } finally {
            rackFactory.finishedWithApplication(app);
        }
    }
}
