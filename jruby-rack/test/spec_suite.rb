def jruby_home
  @jruby_home = File.expand_path(File.dirname(__FILE__) + '/../.jruby')
  require 'java'
  java.lang.System.setProperty("jruby.home", @jruby_home)
end

def specs(filename)
  spec_output = `ruby -S spec -d -f s #{filename}`
  examples = []
  desc_name = nil
  spec_output.each_line do |line|
    if line =~ /^- (.*)/
      examples << "#{desc_name} #{$1}"
    else
      desc_name = line =~ /^(.*)$/ && $1
    end
  end
  examples
end

def discover_examples
  if RUBY_PLATFORM =~ /java/
    require 'jruby/extract'
    JRuby::Extract.new.extract
  end

  @test_dir = File.dirname(__FILE__)
  example_hash = {}
  Dir[@test_dir + '/**/*_spec.rb'].each do |file|
    example_hash[file] = specs(file)
  end
  example_hash
end
