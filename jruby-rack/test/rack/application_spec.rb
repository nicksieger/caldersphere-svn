require File.dirname(__FILE__) + '/../spec_helper'

import org.jruby.rack.DefaultRackApplication unless defined?(DefaultRackApplication)

describe DefaultRackApplication, "call" do
  it "should invoke the call method on the ruby object and return the rack result" do
    servlet_request = mock("servlet request")
    rack_result = org.jruby.rack.RackResult.impl {}

    ruby_object = mock "application"
    ruby_object.should_receive(:call).with(servlet_request).and_return rack_result
    
    application = DefaultRackApplication.new(ruby_object)
    application.call(servlet_request).should == rack_result
  end
end

import org.jruby.rack.DefaultRackApplicationFactory unless defined?(DefaultRackApplicationFactory)

describe DefaultRackApplicationFactory, "newRuntime" do
  it "should create a new Ruby runtime with the rack environment pre-loaded" do
    runtime = DefaultRackApplicationFactory.new.newRuntime
    lambda { runtime.evalScriptlet("Rack") }.should_not raise_error
    lambda { runtime.evalScriptlet("Rack::Servlet") }.should_not raise_error
  end
end

describe DefaultRackApplicationFactory, "newApplication" do
  it "should create a Ruby object from the script snippet given" do
    object = DefaultRackApplicationFactory.new.newApplication("require 'rack/lobster'; Rack::Lobster.new")
    object.respond_to?(:call).should == true
  end
end
