require File.dirname(__FILE__) + '/../spec_helper'

import org.jruby.rack.RackServlet unless defined?(RackServlet)

describe RackServlet, "service" do
  before :each do
    @servlet_context.stub!(:getInitParameter).and_return(
      "require 'rack/lobster'; Rack::Lobster.new")
    @rack_factory = mock("rack factory")
    @servlet = RackServlet.new(@rack_factory)
  end

  it "should delegate to process" do
    request = javax.servlet.http.HttpServletRequest.impl {}
    response = javax.servlet.http.HttpServletResponse.impl {}
    application = mock("application")
    @rack_factory.stub!(:newApplication).and_return application
    @rack_factory.stub!(:finishedWithApplication)
    application.stub!(:call).and_raise "error"
    
    lambda { 
      @servlet.service request, response
    }.should raise_error(javax.servlet.ServletException)
  end
end

describe RackServlet, "process" do
  before :each do
    @servlet_context.stub!(:getInitParameter).and_return(
      "require 'rack/lobster'; Rack::Lobster.new")
    @rack_factory = mock("rack factory")
    @servlet = RackServlet.new(@rack_factory)
  end

  it "should construct a RackApplication and call it" do
    application = mock("application")
    request = mock("request")
    response = mock("response")
    result = mock("rack result")

    @servlet.init(@servlet_config)
    @rack_factory.should_receive(:newApplication).with(
      /Rack::Lobster\.new/).and_return(application)
    @rack_factory.should_receive(:finishedWithApplication).with(application)
    application.should_receive(:call).with(request).and_return result
    result.should_receive(:writeStatus)
    result.should_receive(:writeHeaders)
    result.should_receive(:writeBody)

    @servlet.process(request, response)
  end

  it "should raise a servlet exception if an unexpected exception leaks out" do
    application = mock("application")
    @rack_factory.stub!(:newApplication).and_return application
    @rack_factory.stub!(:finishedWithApplication)
    application.stub!(:call).and_raise "error"
    
    lambda { 
      @servlet.process(mock("request"), mock("response")) 
    }.should raise_error(javax.servlet.ServletException)
  end
end