/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jruby.rack;

import java.util.ArrayList;
import junit.framework.TestCase;
import org.jruby.Main;
import org.jruby.Ruby;
import org.jruby.RubyInstanceConfig;
import org.jruby.javasupport.JavaEmbedUtils;
import org.jruby.runtime.builtin.IRubyObject;

/**
 *
 * @author nicksieger
 */
public class RackSpecRunner extends TestCase {

    private static Ruby specRunnerInstance;
    private static Exception initializationException;
 
    private String fileName;
    
    public RackSpecRunner(String fileName, String testName) {
        super(testName);
        this.fileName = fileName;
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        if (specRunnerInstance == null) {
            try {
                specRunnerInstance = JavaEmbedUtils.initialize(new ArrayList());
                specRunnerInstance.evalScriptlet("require 'rubygems'; gem 'rspec'; require 'spec'");
            } catch (Exception e) {
                initializationException = e;
            }
        }
        
        if (initializationException != null) {
            throw initializationException;
        }
    }

    @Override
    protected void runTest() throws Throwable {
        String runSpecs =
                "argv = ['-f', 's', '-e', '" + getName() + "', '" + fileName + "']"
                + "; $behaviour_runner = Spec::Runner::OptionParser.new.create_behaviour_runner(argv, $stderr, $stdout, true)"
                + "; $behaviour_runner.run(argv, false)";
        IRubyObject result = specRunnerInstance.evalScriptlet(runSpecs);
        if (result.convertToInteger().getLongValue() != 0) {
            fail("Example '" + getName() + "' failed.");
        }
    }
}
