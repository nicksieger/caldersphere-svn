/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jruby.rack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.jruby.Ruby;
import org.jruby.RubyHash;
import org.jruby.javasupport.JavaEmbedUtils;

/**
 *
 * @author nicksieger
 */
public class RackTest extends TestCase {

    public static TestSuite suite() {
        TestSuite suite = new TestSuite("Examples");
        try {
            Ruby tmp = JavaEmbedUtils.initialize(new ArrayList());
            tmp.evalScriptlet("require 'spec_suite'; jruby_home");
            
            Ruby runtime = JavaEmbedUtils.initialize(new ArrayList());
            runtime.getLoadService().require("spec_suite");
            RubyHash rhash = (RubyHash) runtime.evalScriptlet("discover_examples");

            for (Object el : rhash.entrySet()) {
                Entry entry = (Entry) el;
                String filename = entry.getKey().toString();
                for (Object xmp : (List) entry.getValue()) {
                    suite.addTest(new RackSpecRunner(filename, xmp.toString()));
                }
            }
        } catch (Exception e) {
            System.err.println("warning: unable to create test suite");
            e.printStackTrace();
        }
        return suite;
    }
}
