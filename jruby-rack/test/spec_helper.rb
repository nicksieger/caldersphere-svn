require 'rubygems'
gem 'rspec'
require 'spec'

Spec::Runner.configure do |config|
  config.before :each do
    @servlet_context = mock("servlet context")
    @servlet_context.stub!(:log)
    @servlet_config = mock("servlet config")
    @servlet_config.stub!(:getServletName).and_return("A Servlet")
    @servlet_config.stub!(:getServletContext).and_return(@servlet_context)
  end
end