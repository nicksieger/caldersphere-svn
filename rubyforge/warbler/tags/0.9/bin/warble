#!/usr/bin/env ruby
#
#--
# (c) Copyright 2007 Nick Sieger <nicksieger@gmail.com>
# See the file LICENSES.txt included with the distribution for
# software license details.
#++

require 'rubygems'
require 'rake'
require 'warbler'

application = Rake.application
application.standard_exception_handling do
  application.init

  Warbler::Task.new

  task :default => :war

  desc "Generate a configuration file to customize your war assembly"
  task :config do
    if File.exists?(Warbler::Config::FILE) && ENV["FORCE"].nil?
      warn "#{Warbler::Config::FILE}: file already exists. Use FORCE=1 to override"
    else
      cp "#{Warbler::WARBLER_HOME}/generators/warble/templates/warble.rb", Warbler::Config::FILE
    end
  end

  desc "Unpack warbler as a plugin in your Rails application"
  task :pluginize do
    unless Dir["vendor/plugins/warbler*"].empty?
      warn "warbler is already installed as a plugin; please remove before re-installing"
    else
      Dir.chdir("vendor/plugins") do
        ruby "-S", "gem", "unpack", "warbler"
      end
    end
  end

  application.top_level
end