class HelloController < ApplicationController
  def index
    render :text => "It worked<br/>\n\n#{digest}"
  end
  def digest
    OpenSSL::HMAC.hexdigest(OpenSSL::Digest::Digest.new('SHA1'), 'secret07', "it's a secret") 
  end
end
