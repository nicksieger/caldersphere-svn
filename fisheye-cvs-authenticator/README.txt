== ABOUT

This is an implementation of a custom authenticator for FishEye that authenticates against CVS's `passwd' and `users' files.

== BUILDING

In order to build this example FishEye authenticator, you will need the following:

 * a copy of fisheye.jar [1]; and a copy of jruby-complete.jar [2]; both to be
   placed in ./lib
 * Maven 2 -- use the command "mvn install" to build the authenticator.

[1]: http://cenqua.com/fisheye/
[2]: http://headius.blogspot.com/2006/11/advanced-rails-deployment-with-jruby.html

== USAGE

To use with FishEye, first drop the compiled jar along with a copy of jruby-complete.jar in the `lib' directory of your FishEye installation.  In the FishEye admin interface, select a custom authenticator [3].  For the class name, use `org.example.CVSPasswdFisheyeAuthenticator`.  In the properties box, enter values for the following properties:


  CVSROOT=<full path to repository CVSROOT directory>
  DefaultDomain=<default email domain>

[3]: http://cenqua.com/fisheye/doc/1.2/admin/customauth.html

== LICENSE

This code is released under the MIT License.

(c) Copyright 2006 Nick Sieger <nicksieger@gmail.com>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
