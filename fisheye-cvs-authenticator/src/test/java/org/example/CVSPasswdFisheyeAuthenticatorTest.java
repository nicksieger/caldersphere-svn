/*
(c) Copyright 2006 Nick Sieger <nicksieger@gmail.com>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package org.example;

import com.cenqua.fisheye.user.plugin.AuthToken;
import com.cenqua.fisheye.user.plugin.FishEyeAuthenticator;
import java.util.Collections;
import java.util.Properties;
import junit.framework.TestCase;
import org.jruby.Ruby;
import org.jruby.javasupport.JavaEmbedUtils;
import org.jruby.runtime.builtin.IRubyObject;

public class CVSPasswdFisheyeAuthenticatorTest extends TestCase {
    private FishEyeAuthenticator authenticator = new CVSPasswdFisheyeAuthenticator();
    Ruby ruby;
    private String user1, user2;
    private String pass1, pass2, email;
    private String cvsroot;

    protected void setUp() throws Exception {
        super.setUp();
        ruby = JavaEmbedUtils.initialize(Collections.EMPTY_LIST);
        ruby.getLoadService().require("org/example/authenticator");
        ruby.getLoadService().require("org/example/authenticator_test");
        IRubyObject arr = ruby.evalScript("setup");
        String[] userPassFile = (String[]) JavaEmbedUtils.rubyToJava(ruby, arr, String[].class);
        cvsroot = userPassFile[0];
        user1 = userPassFile[1];
        pass1 = userPassFile[2];
        user2 = userPassFile[3];
        pass2 = userPassFile[4];
        email = userPassFile[5];
        authenticator.init(new Properties() {{
            put("CVSROOT", cvsroot);
            put("DefaultDomain", "digitalriver.com");
        }});
    }

    protected void tearDown() throws Exception {
        ruby.evalScript("teardown");
        super.tearDown();
    }

    public void testAuthenticate() {
        assertToken(authenticator.checkPassword(user1, pass1), user1, user1 + "@digitalriver.com");
        assertToken(authenticator.checkPassword(user2, pass2), user2, email);
        assertNull(authenticator.checkPassword("bogus", "pass"));
    }

    public void testRecreateToken() {
        assertToken(authenticator.recreateAuth(user1), user1, user1 + "@digitalriver.com");
        assertToken(authenticator.recreateAuth(user2), user2, email);
        assertNull(authenticator.recreateAuth("bogus"));
    }

    private void assertToken(AuthToken tok, String user, String email) {
        assertEquals(user, tok.getUsername());
        assertEquals(user, tok.getDisplayName());
        assertEquals(email, tok.getEmail());
    }
}
