# (c) Copyright 2006 Nick Sieger <nicksieger@gmail.com>
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

require 'java'
require 'fileutils'

Users = ["someuser", "anotheruser"]
Passwords = ["123123", "456456"]
CustomEmail = "user@domain.tld"
CVSROOT = "#{java.lang.System.getProperty('basedir')}/target"

def setup
  File.open("#{CVSROOT}/passwd", "w") do |f|
    Users.each_with_index do |u, i|
      f << "#{u}:#{CVS::Password.crypt(Passwords[i])}:cvs\n"
    end
  end
  File.open("#{CVSROOT}/users", "w") do |f|
    f << Users[0] << "\n"
    f << Users[1] << ":" << CustomEmail << "\n"
  end
  
  arr = java.lang.String[6].new
  arr[0] = CVSROOT
  arr[1] = Users[0]
  arr[2] = Passwords[0]
  arr[3] = Users[1]
  arr[4] = Passwords[1]
  arr[5] = CustomEmail
  arr
end

def teardown
  FileUtils.rm_f("#{CVSROOT}/passwd")
  FileUtils.rm_f("#{CVSROOT}/users")
end
