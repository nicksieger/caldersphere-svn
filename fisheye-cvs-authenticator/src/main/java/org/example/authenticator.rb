# (c) Copyright 2006 Nick Sieger <nicksieger@gmail.com>
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

require 'java'

module CVS
  class Password
    def self.crypt(password, salt = nil)
      if salt.nil?
        rand_letter = proc { (rand(26) + (rand(2) == 0 ? 65 : 97) ).chr }
        salt = rand_letter.call + rand_letter.call
      end
      password.crypt(salt)
    end
  end

  class AuthToken < com.cenqua.fisheye.user.plugin.AuthToken
    attr_accessor :username, :email, :display_name
    def getDisplayName; display_name; end
    def getUsername; username; end
    def getEmail; email; end
  end

  class Authenticator
    def initialize
      @passwords = Hash[*(IO.readlines("#{$CVSROOT}/passwd").map {|l| l.chomp.split(/:/)[0..1] }.flatten)]
      @emails = Hash[*(IO.readlines("#{$CVSROOT}/users").map do |l| 
        user, email = l.chomp.split(/:/)
        email = "#{user}@#{$DefaultDomain}" unless email
        [user, email]
      end.flatten)]
    end

    # public AuthToken checkPassword(String username, String password)
    def check_password(username, password)
      if password_matches?(username, password)
        recreate_auth(username)
      end
    end

    # public AuthToken recreateAuth(String username)
    def recreate_auth(username)
      if @passwords[username]
        token = CVS::AuthToken.new
        token.username = username
        token.display_name = username
        token.email = @emails[username]
        token
      end
    end

    def password_matches?(username, password)
      crypted = @passwords[username]
      CVS::Password.crypt(password, crypted) == crypted
    end
  end
end