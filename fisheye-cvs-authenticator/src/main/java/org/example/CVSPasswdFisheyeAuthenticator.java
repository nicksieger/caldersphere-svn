/*
(c) Copyright 2006 Nick Sieger <nicksieger@gmail.com>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package org.example;

import com.cenqua.fisheye.user.plugin.AbstractFishEyeAuthenticator;
import com.cenqua.fisheye.user.plugin.AuthToken;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Properties;
import org.jruby.Ruby;
import org.jruby.javasupport.JavaEmbedUtils;
import org.jruby.runtime.builtin.IRubyObject;
import org.jruby.exceptions.RaiseException;

public class CVSPasswdFisheyeAuthenticator extends AbstractFishEyeAuthenticator {
    private Ruby ruby;

    public void init(Properties properties) throws Exception {
        String cvsroot = properties.getProperty("CVSROOT");
        String domain = properties.getProperty("DefaultDomain");
        ruby = JavaEmbedUtils.initialize(Collections.EMPTY_LIST);
        ruby.defineReadonlyVariable("$CVSROOT", JavaEmbedUtils.javaToRuby(ruby, cvsroot));
        ruby.defineReadonlyVariable("$DefaultDomain", JavaEmbedUtils.javaToRuby(ruby, domain));
        ruby.getLoadService().require("org/example/authenticator");
    }

    public void close() {
    }

    public AuthToken checkPassword(String username, String password) {
        return (AuthToken) rubyEval("CVS::Authenticator.new.check_password('" 
            + escape(username) + "', '" + escape(password) + "')", AuthToken.class);
    }

    public AuthToken recreateAuth(String username) {
        return (AuthToken) rubyEval("CVS::Authenticator.new.recreate_auth('" 
            + escape(username) + "')", AuthToken.class);
    }

    public boolean hasPermissionToAccess(AuthToken authToken, String repname, String constraint) {
        return true;
    }

    private synchronized Object rubyEval(String script, Class returnType) {
        try {
            IRubyObject result = ruby.evalScript(script);
            return JavaEmbedUtils.rubyToJava(ruby, result, returnType);
        } catch (RaiseException e) { 
            System.err.println();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.getException().printBacktrace(new PrintStream(baos));
            throw new RuntimeException(e.getException().message.toString() + "\n" + baos.toString());
        }
    }

    private String escape(String val) {
        return val.replaceAll("'", "\\'");
    }
}