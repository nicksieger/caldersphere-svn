require 'rails/info'
require 'rails/info_controller'

module Rails
  module Info
    def self.dynamic_property(name, &value)
      def value.to_s
        self.call.to_s
      rescue Exception
      end
      properties << [name, value]
    end

    def self.formatted_mb(bytes)
      "%0.02f" % (bytes.to_f / 1.megabyte)
    end

    property "Java VM Version" do
      "#{Java::java.lang.System.getProperty 'java.vm.version'} - " +
        "#{Java::java.lang.System.getProperty 'java.vm.vendor'}"
    end

    dynamic_property "Used Memory" do
      used = Java::java.lang.Runtime.getRuntime.totalMemory -
        Java::java.lang.Runtime.getRuntime.freeMemory
      "#{formatted_mb(used)} MB"
    end

    dynamic_property "Total Memory" do
      "#{formatted_mb(Java::java.lang.Runtime.getRuntime.totalMemory)} MB"
    end

    dynamic_property "Free Memory" do
      "#{formatted_mb(Java::java.lang.Runtime.getRuntime.freeMemory)} MB"
    end

    property "Java System Properties", "----"

    Java::java.lang.System.getProperties.keySet.sort.each do |k|
      property k do
        Java::java.lang.System.getProperty k
      end
    end
  end
end

class Rails::InfoController < ActionController::Base
  def gc
    if local_request?
      Java::java.lang.System.gc
      redirect_to :action => "properties"
    else
      render :text => '<p>For security purposes, this information is only available to local requests.</p>', :status => 500
    end
  end
end
