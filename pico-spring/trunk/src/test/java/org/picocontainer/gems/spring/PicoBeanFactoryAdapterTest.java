/*****************************************************************************
 * Copyright (C) PicoContainer Organization. All rights reserved.            *
 * ------------------------------------------------------------------------- *
 * The software in this package is published under the terms of the BSD      *
 * style license a copy of which has been included with this distribution in *
 * the LICENSE.txt file.                                                     *
 *                                                                           *
 * Original code by Nick Sieger <nicksieger@gmail.com>                       *
 *****************************************************************************/

package org.picocontainer.gems.spring;

import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.picocontainer.ComponentAdapter;
import org.picocontainer.PicoContainer;
import org.picocontainer.PicoInitializationException;
import org.picocontainer.defaults.CachingComponentAdapter;
import org.picocontainer.gems.spring.PicoBeanFactoryAdapter;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNotOfRequiredTypeException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

public class PicoBeanFactoryAdapterTest extends MockObjectTestCase {
    Mock mockPico = mock(PicoContainer.class);
    Mock mockAdapter = mock(ComponentAdapter.class);
    PicoBeanFactoryAdapter adapter =
        new PicoBeanFactoryAdapter((PicoContainer) mockPico.proxy());

    public void testGetBeanThrowsNoSuchBeanDefinitionIfKeyIsNotInPicoContainer() {
        mockPico.expects(once()).method("getComponentAdapter")
            .with(eq("key")).will(returnValue(null));
        try {
            adapter.getBean("key");
            fail("expected exception");
        } catch (NoSuchBeanDefinitionException nsbde) {
            // success
        }
    }

    public void testGetBeanThrowsBeansExceptionIfPicoExceptionThrown() {
        mockPico.expects(once()).method("getComponentAdapter")
            .with(eq("key")).will(throwException(new PicoInitializationException("key")));
        try {
            adapter.getBean("key");
            fail("expected exception");
        } catch (BeansException be) {
            // success
        }
    }

    public void testGetBeanCallsGetComponentInstanceOnPicoComponentAdapter() {
        Object dummy = new Object();

        mockPico.expects(once()).method("getComponentAdapter")
            .with(eq("key")).will(returnValue(mockAdapter.proxy()));
        mockAdapter.expects(once()).method("getComponentInstance")
            .with(same(mockPico.proxy())).will(returnValue(dummy));

        assertSame(dummy, adapter.getBean("key"));
    }

    public void testGetBeanWithRequiredTypeThrowsTypeExceptionOnTypeMismatch() {
        Object comp = String.class;

        mockPico.expects(atLeastOnce()).method("getComponentAdapter")
            .with(eq("key")).will(returnValue(mockAdapter.proxy()));
        mockAdapter.expects(atLeastOnce()).method("getComponentInstance")
            .with(same(mockPico.proxy())).will(returnValue(comp));

        assertSame(comp, adapter.getBean("key", Class.class));
        try {
            adapter.getBean("key", String.class);
            fail("expected exception");
        } catch (BeanNotOfRequiredTypeException brte) {
            // success
        }
    }

    public void testContainsBeanTestsExistenceOfComponentAdapter() {
        mockPico.stubs().method("getComponentAdapter").will(returnValue(null));
        mockPico.expects(once()).method("getComponentAdapter").with(eq("key")).will(returnValue(mockAdapter.proxy()));

        assertTrue(adapter.containsBean("key"));
        assertFalse(adapter.containsBean("missingKey"));
    }

    public void testIsSingletonTestsIfAdapterIsACachingComponentAdapter() {
        mockPico.stubs().method("getComponentAdapter").with(eq("cached"))
            .will(returnValue(new CachingComponentAdapter((ComponentAdapter) mockAdapter.proxy())));
        mockPico.stubs().method("getComponentAdapter").with(eq("key"))
            .will(returnValue(mockAdapter.proxy()));

        assertTrue(adapter.isSingleton("cached"));
        assertFalse(adapter.isSingleton("key"));
    }

    public void testGetTypeReturnsImplementationOfAdapter() {
        mockPico.expects(once()).method("getComponentAdapter").with(eq("key"))
            .will(returnValue(mockAdapter.proxy()));
        mockAdapter.stubs().method("getComponentImplementation").will(returnValue(String.class));

        assertEquals(String.class, adapter.getType("key"));
    }

    public void testPicoDoesNotSupportAliasesSoEmptyStringArrayIsReturnedForGetAliases() {
        mockPico.expects(once()).method("getComponentAdapter").with(eq("key"))
            .will(returnValue(mockAdapter.proxy()));
        assertEquals(0, adapter.getAliases("key").length);
    }

}
