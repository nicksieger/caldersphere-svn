/*****************************************************************************
 * Copyright (C) PicoContainer Organization. All rights reserved.            *
 * ------------------------------------------------------------------------- *
 * The software in this package is published under the terms of the BSD      *
 * style license a copy of which has been included with this distribution in *
 * the LICENSE.txt file.                                                     *
 *                                                                           *
 * Original code by Nick Sieger <nicksieger@gmail.com>                       *
 *****************************************************************************/

package org.picocontainer.gems.spring;

import java.beans.PropertyEditor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.picocontainer.ComponentAdapter;
import org.picocontainer.PicoContainer;
import org.picocontainer.PicoException;
import org.picocontainer.defaults.CachingComponentAdapter;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanNotOfRequiredTypeException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class PicoBeanFactoryAdapter implements PicoBeanFactory {

    private PicoContainer pico;

    public PicoBeanFactoryAdapter(PicoContainer pico) {
        this.pico = pico;
    }

    public Object getBean(String name) throws BeansException {
        return getBean(name, null);
    }

    public Object getBean(String name, Class requiredType)
        throws BeansException {
        try {
            Object result = getComponentAdapter(name).getComponentInstance(pico);
            if (requiredType != null && !requiredType.isInstance(result)) {
                throw new BeanNotOfRequiredTypeException(name, requiredType, result.getClass());
            }
            return result;
        } catch (PicoException pe) {
            throw new FatalBeanException("unable to get bean " + name, pe);
        }
    }

    public boolean containsBean(String name) {
        return pico.getComponentAdapter(name) != null;
    }

    public boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
        return getComponentAdapter(name) instanceof CachingComponentAdapter;
    }

    public Class getType(String name) throws NoSuchBeanDefinitionException {
        return getComponentAdapter(name).getComponentImplementation();
    }

    public String[] getAliases(String name) throws NoSuchBeanDefinitionException {
        getComponentAdapter(name);
        return new String[0];
    }

    public int getBeanDefinitionCount() {
        return getComponentAdaptersWithStringKeys(Object.class).size();
    }

    public String[] getBeanDefinitionNames() {
        return getBeanDefinitionNames(Object.class);
    }

    public String[] getBeanDefinitionNames(Class type) {
        List compAdapters = getComponentAdaptersWithStringKeys(type);
        String[] names = new String[compAdapters.size()];
        int count = 0;
        for (Iterator i = compAdapters.iterator(); i.hasNext();) {
            ComponentAdapter compAdapter = (ComponentAdapter) i.next();
            names[count++] = compAdapter.getComponentKey().toString();
        }
        return names;
    }

    public boolean containsBeanDefinition(String name) {
        return containsBean(name);
    }

    public Map getBeansOfType(Class type) throws BeansException {
        Map beans = new HashMap();
        for (Iterator i = getComponentAdaptersWithStringKeys(type).iterator(); i.hasNext();) {
            ComponentAdapter compAdapter = (ComponentAdapter) i.next();
            beans.put(compAdapter.getComponentKey(), compAdapter.getComponentInstance(pico));
        }
        return beans;
    }

    public Map getBeansOfType(Class type, boolean includePrototypes, boolean includeFactoryBeans)
        throws BeansException {
        return getBeansOfType(type);
    }

    public BeanFactory getParentBeanFactory() {
        PicoContainer parent = pico.getParent();
        if (parent != null) {
            return new PicoBeanFactoryAdapter(parent);
        }
        return null;
    }

    // Spring 1.2
    public String[] getBeanNamesForType(Class type) {
        return getBeanDefinitionNames(type);
    }

    // Spring 1.2
    public String[] getBeanNamesForType(Class type, boolean includePrototypes, boolean includeFactoryBeans) {
        return getBeanNamesForType(type);
    }

    private ComponentAdapter getComponentAdapter(String name) {
        ComponentAdapter compAdapter = pico.getComponentAdapter(name);
        if (compAdapter == null) {
            throw new NoSuchBeanDefinitionException(name, "bean not registered");
        }
        return compAdapter;
    }

    private List getComponentAdaptersWithStringKeys(Class ofType) {
        List stringKeyCompAdapters = new ArrayList();
        for (Iterator i = pico.getComponentAdapters().iterator(); i.hasNext();) {
            ComponentAdapter compAdapter = (ComponentAdapter) i.next();
            if (compAdapter.getComponentKey() instanceof String
                && ofType.isAssignableFrom(compAdapter.getComponentImplementation())) {
                stringKeyCompAdapters.add(compAdapter);
            }
        }
        return stringKeyCompAdapters;
    }

	public Object autowire(Class beanClass, int autowireMode, boolean dependencyCheck) throws BeansException {
		// TODO Auto-generated method stub
		return null;
	}

	public void autowireBeanProperties(Object existingBean, int autowireMode, boolean dependencyCheck) throws BeansException {
		// TODO Auto-generated method stub

	}

	public void applyBeanPropertyValues(Object existingBean, String name) throws BeansException {
		// TODO Auto-generated method stub

	}

	public Object applyBeanPostProcessorsBeforeInitialization(Object existingBean, String name) throws BeansException {
		// TODO Auto-generated method stub
		return null;
	}

	public Object applyBeanPostProcessorsAfterInitialization(Object existingBean, String name) throws BeansException {
		// TODO Auto-generated method stub
		return null;
	}

	public void ignoreDependencyType(Class arg0) {
		// TODO Auto-generated method stub

	}

	public void ignoreDependencyInterface(Class arg0) {
		// TODO Auto-generated method stub

	}

	public BeanDefinition getBeanDefinition(String arg0) throws NoSuchBeanDefinitionException {
		//Bean
		return null;
	}

	public void preInstantiateSingletons() throws BeansException {
		// TODO Auto-generated method stub

	}

	public void setParentBeanFactory(BeanFactory arg0) {
		// TODO Auto-generated method stub

	}

	public void registerCustomEditor(Class arg0, PropertyEditor arg1) {
		// TODO Auto-generated method stub

	}

	public void addBeanPostProcessor(BeanPostProcessor arg0) {
		// TODO Auto-generated method stub

	}

	public int getBeanPostProcessorCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void registerAlias(String arg0, String arg1) throws BeansException {
		// TODO Auto-generated method stub

	}

	public void registerSingleton(String arg0, Object arg1) throws BeansException {
		// TODO Auto-generated method stub

	}

	public boolean containsSingleton(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public void destroySingletons() {
		// TODO Auto-generated method stub

	}
}
