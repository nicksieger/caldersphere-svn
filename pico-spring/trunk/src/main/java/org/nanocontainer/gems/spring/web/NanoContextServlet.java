/*****************************************************************************
 * Copyright (C) PicoContainer Organization. All rights reserved.            *
 * ------------------------------------------------------------------------- *
 * The software in this package is published under the terms of the BSD      *
 * style license a copy of which has been included with this distribution in *
 * the LICENSE.txt file.                                                     *
 *                                                                           *
 * Original code by Nick Sieger <nicksieger@gmail.com>                       *
 *****************************************************************************/

package org.nanocontainer.gems.spring.web;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderServlet;

public class NanoContextServlet extends ContextLoaderServlet {

    protected ContextLoader createContextLoader() {
        return new NanoContextLoader();
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

}
