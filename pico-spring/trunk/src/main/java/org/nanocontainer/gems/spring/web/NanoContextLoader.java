/*****************************************************************************
 * Copyright (C) PicoContainer Organization. All rights reserved.            *
 * ------------------------------------------------------------------------- *
 * The software in this package is published under the terms of the BSD      *
 * style license a copy of which has been included with this distribution in *
 * the LICENSE.txt file.                                                     *
 *                                                                           *
 * Original code by Nick Sieger <nicksieger@gmail.com>                       *
 *****************************************************************************/

package org.nanocontainer.gems.spring.web;

import javax.servlet.ServletContext;

import org.nanocontainer.NanoContainer;
import org.nanocontainer.nanowar.ApplicationScopeObjectReference;
import org.nanocontainer.nanowar.KeyConstants;
import org.picocontainer.PicoContainer;
import org.picocontainer.defaults.ObjectReference;
import org.picocontainer.gems.spring.PicoApplicationContextAdapter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

public class NanoContextLoader extends ContextLoader {

    private ObjectReference containerReference;

    public NanoContextLoader() {
        containerReference = null;
    }

    public NanoContextLoader(ObjectReference ref) {
        this.containerReference = ref;
    }

    protected ApplicationContext loadParentContext(ServletContext servletContext) throws BeansException {
        if (containerReference == null) {
            containerReference = new ApplicationScopeObjectReference(servletContext, KeyConstants.APPLICATION_CONTAINER);
        }

        Object container = containerReference.get();
        PicoContainer pico = null;

        if (container instanceof PicoContainer) {
            pico = (PicoContainer) container;
        } else if (container instanceof NanoContainer) {
            pico = ((NanoContainer) container).getPico();
        }

        if (pico != null) {
            return new PicoApplicationContextAdapter(pico);
        }

        return null;
    }
}
