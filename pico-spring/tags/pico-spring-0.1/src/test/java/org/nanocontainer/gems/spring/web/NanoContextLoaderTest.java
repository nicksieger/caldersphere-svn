/*****************************************************************************
 * Copyright (C) PicoContainer Organization. All rights reserved.            *
 * ------------------------------------------------------------------------- *
 * The software in this package is published under the terms of the BSD      *
 * style license a copy of which has been included with this distribution in *
 * the LICENSE.txt file.                                                     *
 *                                                                           *
 * Original code by Nick Sieger <nicksieger@gmail.com>                       *
 *****************************************************************************/

package org.nanocontainer.gems.spring.web;

import javax.servlet.ServletContext;

import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.nanocontainer.NanoContainer;
import org.nanocontainer.gems.spring.web.NanoContextLoader;
import org.nanocontainer.nanowar.KeyConstants;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.PicoContainer;
import org.picocontainer.defaults.ObjectReference;

public class NanoContextLoaderTest extends MockObjectTestCase {
    Mock mockContext = mock(ServletContext.class);

    ServletContext servletContext = (ServletContext) mockContext.proxy();

    public void testLoadParentReturnsNullWhenObjectReferenceDoesNotYieldAPicoOrNanoContainer() {
        Mock mockRef = mock(ObjectReference.class);
        mockRef.stubs().method("get").will(returnValue(null));
        NanoContextLoader loader = new NanoContextLoader((ObjectReference) mockRef.proxy());
        assertNull(loader.loadParentContext(servletContext));
    }

    public void testLoadParentContextWithExistingReference() {
        Mock mockRef = mock(ObjectReference.class);
        Mock mockPico = mock(PicoContainer.class);

        mockRef.stubs().method("get").will(returnValue(mockPico.proxy()));

        NanoContextLoader loader = new NanoContextLoader((ObjectReference) mockRef.proxy());
        assertNotNull(loader.loadParentContext(servletContext));
    }

    public void testLoadParentContextFromApplicationScopeReference() {
        Mock mockPico = mock(MutablePicoContainer.class);
        Mock mockNano = mock(NanoContainer.class);
        mockContext.stubs().method("getAttribute").with(eq(KeyConstants.APPLICATION_CONTAINER))
            .will(returnValue(mockNano.proxy()));
        mockNano.stubs().method("getPico").will(returnValue(mockPico.proxy()));
        NanoContextLoader loader = new NanoContextLoader();
        assertNotNull(loader.loadParentContext(servletContext));
    }
}
