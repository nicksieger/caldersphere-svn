/*****************************************************************************
 * Copyright (C) PicoContainer Organization. All rights reserved.            *
 * ------------------------------------------------------------------------- *
 * The software in this package is published under the terms of the BSD      *
 * style license a copy of which has been included with this distribution in *
 * the LICENSE.txt file.                                                     *
 *                                                                           *
 * Original code by Nick Sieger <nicksieger@gmail.com>                       *
 *****************************************************************************/

package org.picocontainer.gems.spring;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public interface PicoBeanFactory extends ConfigurableListableBeanFactory {
}
