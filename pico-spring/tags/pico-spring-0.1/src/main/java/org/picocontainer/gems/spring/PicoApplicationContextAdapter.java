/*****************************************************************************
 * Copyright (C) PicoContainer Organization. All rights reserved.            *
 * ------------------------------------------------------------------------- *
 * The software in this package is published under the terms of the BSD      *
 * style license a copy of which has been included with this distribution in *
 * the LICENSE.txt file.                                                     *
 *                                                                           *
 * Original code by Nick Sieger <nicksieger@gmail.com>                       *
 *****************************************************************************/

package org.picocontainer.gems.spring;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import org.picocontainer.PicoContainer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.io.Resource;

public class PicoApplicationContextAdapter implements ApplicationContext {

    private PicoBeanFactory beanFactory;

    public PicoApplicationContextAdapter(PicoContainer pico) {
        this(new PicoBeanFactoryAdapter(pico));
    }

    public PicoApplicationContextAdapter(PicoBeanFactory factory) {
        this.beanFactory = factory;
    }

    public boolean containsBean(String name) {
        return beanFactory.containsBean(name);
    }

    public boolean containsBeanDefinition(String name) {
        return beanFactory.containsBeanDefinition(name);
    }

    public String[] getAliases(String name) throws NoSuchBeanDefinitionException {
        return beanFactory.getAliases(name);
    }

    public Object getBean(String name, Class requiredType) throws BeansException {
        return beanFactory.getBean(name, requiredType);
    }

    public Object getBean(String name) throws BeansException {
        return beanFactory.getBean(name);
    }

    public int getBeanDefinitionCount() {
        return beanFactory.getBeanDefinitionCount();
    }

    public String[] getBeanDefinitionNames() {
        return beanFactory.getBeanDefinitionNames();
    }

    public String[] getBeanDefinitionNames(Class type) {
        return beanFactory.getBeanDefinitionNames(type);
    }

    public Map getBeansOfType(Class type, boolean includePrototypes, boolean includeFactoryBeans) throws BeansException {
        return beanFactory.getBeansOfType(type, includePrototypes, includeFactoryBeans);
    }

    public Map getBeansOfType(Class type) throws BeansException {
        return beanFactory.getBeansOfType(type);
    }

    public BeanFactory getParentBeanFactory() {
        return beanFactory.getParentBeanFactory();
    }

    public Class getType(String name) throws NoSuchBeanDefinitionException {
        return beanFactory.getType(name);
    }

    public boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
        return beanFactory.isSingleton(name);
    }

    public String[] getBeanNamesForType(Class type, boolean includePrototypes, boolean includeFactoryBeans) {
        return beanFactory.getBeanNamesForType(type, includePrototypes, includeFactoryBeans);
    }

    public String[] getBeanNamesForType(Class type) {
        return beanFactory.getBeanNamesForType(type);
    }

    public ApplicationContext getParent() {
        PicoBeanFactory parentBeanFactory = (PicoBeanFactory) beanFactory.getParentBeanFactory();
        if (parentBeanFactory != null) {
            return new PicoApplicationContextAdapter(parentBeanFactory);
        }
        return null;
    }

    public String getDisplayName() {
        return "PicoApplicationContextAdapter";
    }

    public long getStartupDate() {
        return 0;
    }

    public void publishEvent(ApplicationEvent event) {
    }

    public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
        return null;
    }

    public String getMessage(String code, Object[] args, Locale locale)
        throws NoSuchMessageException {
        return null;
    }

    public String getMessage(MessageSourceResolvable resolvable, Locale locale)
        throws NoSuchMessageException {
        return null;
    }

    public Resource getResource(String location) {
        return null;
    }

    public Resource[] getResources(String locationPattern) throws IOException {
        return null;
    }
}
