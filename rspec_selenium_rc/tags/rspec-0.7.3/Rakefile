require 'spec/rake/spectask'

task :default => [:test]
task :test => [:specs]
task :specs => ['spec:acceptance']

namespace :spec do
  # Sets up any servers needed for acceptance specs
  task :acceptance_setup

  # Add prerequisites to this task to have them run as acceptance tests
  task :acceptance_run

  # Tears down any servers needed for acceptance specs
  task :acceptance_teardown

  task :acc => :acceptance_specs

  # The acceptance specs task
  Spec::Rake::SpecTask.new(:acceptance_specs) do |t|
    if ENV['SPECS']
      t.spec_files = FileList["spec/#{ENV['SPECS']}"]
    else
      t.spec_files = FileList['spec/**/*_spec.rb']
    end
    t.ruby_opts << "-Ilib"
  end

  # Setup the server and run the acceptance specs
  task :acceptance_cycle do
    begin
      Rake::Task['spec:acceptance_setup'].invoke
      sleep 1
      Rake::Task['spec:acceptance_run'].invoke
    ensure
      Rake::Task['spec:acceptance_teardown'].invoke
    end
  end

  desc "Execute the acceptance specs"
  task :acceptance do
    Rake::Task['spec:acceptance_run'].prerequisites << 'spec:acceptance_specs'
    Rake::Task['spec:acceptance_cycle'].invoke
  end
end

task :ie do
  ENV['SPEC_BROWSER'] = '*iexplore'
end

task :start_selenium do
  Thread.new { system "java -jar \"lib/selenium-server.jar\"" }
  # Make sure selenium rc server is up before continuing
  tries = 3
  begin
    Net::HTTP.get('localhost', '/selenium-server/SeleneseRunner.html', 4444)
  rescue
    sleep 1
    tries -= 1
    retry if tries > 0
  end
end

task :stop_selenium do
  require 'net/http'
  Net::HTTP.get('localhost', '/selenium-server/driver/?cmd=shutDown', 4444) rescue nil
end

namespace :spec do
  task :acceptance_setup => [:start_selenium]
  task :acceptance_teardown => [:stop_selenium]
end
