require 'uri'
require 'rubygems'
require 'spec'
require 'selenium'

module Selenium
  class SeleneseInterpreter
    alias :enter :type # type clashes with Object#type so we can't delegate it

    def click_and_wait(locator, timeout="5000")
      click(locator)
      wait_for_page_to_load timeout
    end

    def open_and_wait(url, timeout="5000")
      open(url)
      wait_for_page_to_load timeout
    end
  end
end

module ServerConfigHelper
  module ContextMethods
    def server_vars(options)
      setup do
        @server_vars ||= {}
        @server_vars.update(options)
      end
    end

    def server_scheme(scheme)
      server_vars "scheme" => scheme
    end

    def server_host(host)
      server_vars "host" => host
    end

    def server_port(port)
      server_vars "port" => port
    end

    def server_url(url)
      uri = URI.parse(url)
      server_vars "scheme" => uri.scheme, "host" => uri.host, "port" => uri.port
    end
  end

  module SpecMethods
    def server_vars
      @server_vars ||= {}
    end

    def server_scheme
      server_vars["scheme"] || 'http'
    end

    def server_host
      server_vars["host"] || ENV['SPEC_HOST'] || 'localhost'
    end

    def server_port
      server_vars["port"] || (ENV['SPEC_PORT'] && ENV['SPEC_PORT'].to_i) || 80
    end

    def server_url
      "#{server_scheme}://#{server_host}:#{server_port}"
    end
  end
end

module SeleniumHelper
  module ContextMethods
    def self.extended(base)
      base.teardown do
        if @selenium
          @selenium.stop
          @selenium = nil
        end
      end
    end
  end

  module SpecMethods
    def self.included(base)
      Selenium::SeleneseInterpreter.public_instance_methods.each do |meth|
        base.module_eval %{
          def #{meth}(*args, &block)
            selenium.send(:#{meth}, *args, &block)
          end
        } unless base.public_instance_methods.include? meth
      end
    end

    def browser
      ENV["SPEC_BROWSER"] || "*firefox"
    end

    def selenium
      unless @selenium
        @selenium = Selenium::SeleneseInterpreter.new("localhost", 4444, browser, server_url, 5000)
        @selenium.start
      end
      @selenium
    end
  end
end  

class Spec::Runner::Context
  def before_context_eval
    @context_eval_module.include ServerConfigHelper::SpecMethods
    @context_eval_module.include SeleniumHelper::SpecMethods
    @context_eval_module.extend ServerConfigHelper::ContextMethods
    @context_eval_module.extend SeleniumHelper::ContextMethods
  end
end
