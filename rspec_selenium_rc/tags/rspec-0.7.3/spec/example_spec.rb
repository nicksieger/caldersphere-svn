require 'spec_helper'

context "Google" do
  server_host "www.google.com"

  specify "search for Selenium RC" do
    open server_url
    enter "name=q", "selenium rc"
    click_and_wait "name=btnG"
    get_body_text.should_match /Selenium Remote Control/
  end
end