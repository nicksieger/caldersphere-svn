require 'rubygems'
require 'spec'
require 'selenium'

module Selenium
  class SeleneseInterpreter
    alias :enter :type # type clashes with Object#type so we can't delegate it

    def click_and_wait(locator, timeout="5000")
      click(locator)
      wait_for_page_to_load timeout
    end

    def open_and_wait(url, timeout="5000")
      open(url)
      wait_for_page_to_load timeout
    end
  end
end

module SeleniumHelper
  def self.included(base)
    Selenium::SeleneseInterpreter.public_instance_methods.each do |meth|
      base.module_eval %{
        def #{meth}(*args, &block)
          selenium.send(:#{meth}, *args, &block)
        end
      } unless base.public_instance_methods.include? meth
    end
  end

  def teardown
    super
    if @selenium
      @selenium.stop
      @selenium = nil
    end
  end

  def selenium
    unless @selenium
      @selenium = Selenium::SeleneseInterpreter.new("localhost", 4444, browser, server_url, 5000)
      @selenium.start
    end
    @selenium
  end
end  
