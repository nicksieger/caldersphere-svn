require 'rspec_selenium'
require 'test/unit'
require 'uri'

class AcceptanceSpecTestCase < Test::Unit::TestCase
  def run(*args)
  end

  def setup_with_fixtures; end

  def self.server_scheme(scheme)
    @@scheme = scheme
  end

  def self.server_host(host)
    @@host = host
  end

  def self.server_port(port)
    @@port = port
  end

  def self.server_url(url)
    uri = URI.parse(url)
    server_scheme uri.scheme
    server_host uri.host
    server_port uri.port
  end

  def server_scheme
    @@scheme ||= 'http'
  end

  def server_host
    @@host ||= ENV['SPEC_HOST'] || 'localhost'
  end

  def server_port
    @@port ||= (ENV['SPEC_PORT'] && ENV['SPEC_PORT'].to_i) || 80
  end

  def server_url
    "#{server_scheme}://#{server_host}:#{server_port}"
  end

  def browser
    ENV["SPEC_BROWSER"] || "*firefox"
  end

  include SeleniumHelper
end

module Spec
  module Runner
    class Context
      def before_context_eval
        inherit AcceptanceSpecTestCase
      end
    end
  end
end

Test::Unit.run = true
